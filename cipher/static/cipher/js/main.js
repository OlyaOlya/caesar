/**
 * Created by Администратор on 20.02.2017.
 */

jQuery(document).ready(function ($) {

    $('#left').click(function() {
        var text = $("#data1").val()
        var rot = $("#rot").val()

        $.ajax({
            url: '/cipher/encrypt/',
            data: {
                'text': text, 'rot': rot
            },
            dataType: 'json',
            success: function (data) {
                $("#data2").val(data.is)
            }
        });
    });

    $('#right').click(function() {
        var text = $("#data1").val()
        var rot = $("#rot").val()

        $.ajax({
            url: '/cipher/decrypt/',
            data: {
                'text': text, 'rot': rot
            },
            dataType: 'json',
            success: function (data) {
                $("#data2").val(data.is)
            }
        });
    });



    //$('#data1').keyup(function(){
    $('#data1').keyup(function(){
        $("#diagram").css({"display": "block"});
        var text = $(this).val()

        $.ajax({
            url: '/cipher/diagram/',
            data: {
                    'text': text
            },
            dataType: 'json',
            success: function (data) {
                var letter = (data.letters)
                var serie = (data.seriess)

            var data={
                labels: letter,
                series: [
                        serie
                ]
            }

            var options={

                ticks: [1, 10, 20, 30],
                height: 200,
                seriesBarDistance: 6,
                axisX: {
                    showGrid: false,
                    labelInterpolationFnc: function(value) {
                        return value[0];
                    }
                }

            }

            var responsiveOptions=
            [
              ['screen and (min-width: 300px)', {
                    seriesBarDistance: 15,
                    axisX: {
                        labelInterpolationFnc: function(value) {
                            return value.slice(0, 3);
                        }
                    }
              }],
              ['screen and (min-width: 600px)', {
                    seriesBarDistance: 30,
                    axisX: {
                        labelInterpolationFnc: Chartist.noop
                    }
              }]
            ];

            new Chartist.Bar('.ct-chart', data, options, responsiveOptions);
            }

        });
    });

    //окно
    $(".info a").click(function(e){
        e.preventDefault();
        $(this).parent().fadeOut();

    });

//валидация
    $('#data1').on('keyup keypress', function(e) {

        if (e.keyCode == 8 || e.keyCode == 46 || e.keyCode == 16 || e.keyCode == 17) {}
        else
        {
           var letters=' 1234567890,.()@!:;\'\"[]{}zxcvbnmasdfghjklqwertyuiopQWERTYUIOPLKJHGFDSAZXCVBNM-';
           if (letters.indexOf(String.fromCharCode(e.which))==-1) {
               $("#data1").css({"border": "3px solid red", "box-shadow": "1px 1px 2px 0 red"});
               $("#er").text("*Текст должен содержать только английские буквы")
           }
        }
    });
    $('#data1').on('blur click', function(e) {

       k=0
       var a = jQuery("#data1").val();
       b=a.replace(/[^a-zA-ZА-Яа-я]/g, '')
       c= b.split('');
        var count = c.length;
        for (var i = 0; i < count; i++) {
            var letters = ' 1234567890,.()@!:;\'\"[]{}zxcvbnmasdfghjklqwertyuiopQWERTYUIOPLKJHGFDSAZXCVBNM-';
            if(letters.indexOf((c[i])) == -1){
                k++;
            }
        }

        if (k>0){
            $("#data1").css({"border": "3px solid red", "box-shadow": "1px 1px 2px 0 red"});
            $("#er").text("*Текст должен содержать только английские буквы");
            $("#left").attr("disabled",true);
            $("#right").attr("disabled","disabled");
        }
        else{
             $("#data1").css({"border": "1px solid grey", "box-shadow": "none"});
             $("#er").text("");
             $("#left").removeAttr("disabled");
             $("#right").removeAttr("disabled");
        }

    });


   // function ref(){
    //$('#data1').on('blur keyup keypress', function(e) {
    $('#data1').on('blur', function(e) {

        if($("#fr").is(":checked")) {
           // alert('отмечен');

            $(".info").css({"display": "block", "background": "#FFB6C1"});

            var text = $("#data1").val()

            $.ajax({
                url: '/cipher/frequency/',
                data: {
                    'text': text
                },
                dataType: 'json',
                success: function (data) {
                    $("#message").text(data.res)

                }
            });
        }

        if($("#dic").is(":checked")){
            //var isAjaxBusy = false;

            $(".info").css({"display": "block" , "background": "#00FF7F"});
             var text = $("#data1").val()

                    $.ajax({

            /*beforeSend:function(){
            if(isAjaxBusy){
              return false;

              }
              else
              {
              isAjaxBusy = true;
            }
            },*/
             url: '/cipher/selection_rotate/',
             data: {
                    'text': text//, 'rot': rot
            },
            dataType: 'json',
          success: function (data) {
              //  $("#message").val(data.res)
              $("#message").text(data.res)
              //alert(data.count)


        }/*,
                    complete:function(){
            isAjaxBusy = false;

}*/

            });

       }
    });
   // }

});


