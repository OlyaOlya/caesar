from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'encrypt/$', views.encrypt, name='encrypt'),
    url(r'decrypt/$', views.decrypt, name='decrypt'),
    url(r'diagram/$', views.diagram, name='diagram'),
    url(r'selection_rotate/$', views.selection_rotate, name='selection_rotate'),
    url(r'frequency/$', views.frequency, name='frequency'),

]