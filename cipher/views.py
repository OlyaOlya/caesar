from django.shortcuts import render
import json
from django.http import HttpResponse, JsonResponse
import re
import enchant
import random
#from nltk.corpus import words

def index(request):
    return render(request, 'cipher/index.html')

######################################
#шифрует текст
def encrypt(request):
    text = request.GET.get('text', None)
    rot = request.GET.get('rot', None)
    rot = int(rot)*(-1)
    res = dec(text, rot)
    data = {
        'is': res
    }
    return JsonResponse(data)

#################################
#расшифровует текст
def decrypt(request):
    text = request.GET.get('text', None)
    rot = request.GET.get('rot', None)
    res = dec(text, rot)
    data = {
         'is': res
        }
    return JsonResponse(data)

#############################################
#функция шифрования
def dec(text, rot):
    alpha = 'abcdefghijklmnopqrstuvwxyz'
    ALPHA = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    n = int(rot)
    text = (str(text)).strip()
    res = ''
    for c in text:
        if c in alpha:
            res += alpha[(alpha.index(c) - n) % len(alpha)]
        elif c in ALPHA:
            res += ALPHA[(ALPHA.index(c) - n) % len(ALPHA)]
        else:
            res += c
    return res

###############################################
#проверка на зашифрованность
def frequency(request):
    #теоритическая частота
    fre = {'a': 7.96,
           'b': 1.60,
           'c': 2.84,
           'd': 4.01,
           'e': 12.86,
           'f': 2.62,
           'g': 1.99,
           'h': 5.39,
           'i': 7.77,
           'j': 0.16,
           'k': 0.41,
           'l': 3.51,
           'm': 2.43,
           'n': 7.51,
           'o': 6.62,
           'p': 1.81,
           'q': 0.17,
           'r': 6.83,
           's': 6.62,
           't': 9.72,
           'u': 2.48,
           'v': 1.15,
           'w': 1.80,
           'x': 0.17,
           'y': 1.52,
           'z': 0.05
    }
    #максимальная теоритическая частота
    maxfr1 = {
                'e': 12.86,
                't': 9.72,
                'a': 7.96,
                'i': 7.77,
                'n': 7.51,
                'r': 6.83,
                's': 6.62,
                'o': 6.62
    }
    #минимальная теоритическая частота
    minfr1 = {
        'q': 0.17,
        'k': 0.41,
        'j': 0.16,
        'x': 0.17,
        'z': 0.05
    }

    text = request.GET.get('text', None)
    text = text.lower()
    text1 = re.sub(u"[^a-z]", "", text)
    lst = list(''.join(text1.split()))
    dct = {}

    #расчет частоты в тексте
    for i in lst:
        counter = dct.get(i, 0)
        counter += 1
        dct[i] = counter

    for i in dct:
        dct[i] = round((dct[i]*100/len(text1)), 2)

    #получить список ключей по значению
    def get_key(d, value):
        l = []
        for k, v in d.items():
            if v == value:
                l.append(k)
        return l

    #поиск мин и макс
    def maxmin(dct1):
        res = list(dct1.values());
        minvalue = 100
        maxvalue = -100
        y = 0
        while y < len(res):
            if res[y] <= minvalue:
                minvalue = res[y]
            if res[y] >= maxvalue:
                maxvalue = res[y]
            y += 1
        return maxvalue, minvalue

    #мин и макс
    maxV, minV = maxmin(dct)
    dmaxV, dminV = maxmin(fre)

    minK = get_key(dct, minV)
    maxK = get_key(dct, maxV)

    #dminK = get_key(fre, dminV)
    dmaxK = get_key(fre, dmaxV)

    maxfr2= list(maxfr1.keys())
    minfr2 = list(minfr1.keys())

    maxset = list(set(maxfr2) & set(maxK))
    minset = list(set(minfr2) & set(minK))

    #если нет букв с теор мин частотой
    count = 0
    for k, v in minfr1.items():
        if k in list(dct.keys()):
            count += 1
    n = -1

    if (minset != [] or count == 0) and maxset != []:
        c = "Данный текст не зашифрован! Зашифруйте его :)"
    elif len(text)<20:
        c = "Хм.. Слишком мало текста для определения смещения "
    else:
        n = 0
        alpha = 'abcdefghijklmnopqrstuvwxyz'
        alpha.lower()
        s = str(maxK[0])
        s1 = str(dmaxK[0])
        n = alpha.index(s)-alpha.index(s1)
        n = str(n)
        c = "Текст зашифрован :( Скорее всего со смещением - " + n

    data = {
        'res': c
    }
    return JsonResponse(data)

####################################
#рисует диаграмму
def diagram(request):
    text = request.GET.get('text', None)
    text = text.lower()
    text1 = re.sub(u"[^a-z]", "", text)
    lst = list(''.join(text1.split()))
    res = []
    dct = {}

    #считает количесвто каждой буквы в тексте
    for i in lst:
        counter = dct.get(i, 0)
        counter += 1
        dct[i] = counter

    #считает частоту каждой буквы в тексте
    for i in dct:
        dct[i] = round((dct[i]*100/len(text1)), 2)

    letters = list(dct.keys())
    seriess = list(dct.values())
    data = {
        'letters': letters,
        'seriess': seriess
    }
    return JsonResponse(data)


###############################################
#попытка угадать смещение через enchant
def selection_rotate(request):
    n = 0
    text = request.GET.get('text', None)
    rot = 1
    c = ""

    def allf(text1, n):
        counter, partcount = compare(text1)
        res, new, n2 = inspection(counter, partcount, n)
        if new == "":
            return res
        #else:
            #return allf(new, n)
        else:
            result = allf(new, n2)
        return result

    def compare(texttext):
        d = enchant.Dict("en_US")
        #text1 = re.sub('[^A-Za-z\s]*', '', text)
        texttext = (str(texttext)).strip()
        texttext = texttext.lower()
        text = re.sub(u"[^a-z ]", "", texttext)

        i = 0
        counter = 0
        partcount = round((len(text.split()) * 20) / 100 + 0.5)

        text1 = text.split()
        while i < partcount:
            i += 1
            r = random.choice(text1)
            if len(r) >= 2:
                if d.check(r):
                    counter += 1
                #else:
                    #i -= 1
            if i == partcount:
                break
        return counter, partcount

    #counter, partcount = compare(text)

    def inspection(counter, partcount, n1):
        sms = ""
        if int(counter) > partcount * 0.8 and n1 == 0:
            sms = "Данный текст не зашифрован!"
            n1=0
            newtext=""
            #return c
            #counter=0
            #if int(counter) <= partcount * 0.8:
            #pass
        elif int(counter) > partcount * 0.8:
            #n=n+1
            sms = "Скорее всего со смещением - " + str(n1)
            newtext =""
            #return c
        elif n1<26:
            sms=""
            newtext = str(dec(text, 1))
            n1 += 1
        else:
            sms = "Хм.. не могу подобрать смещение"

        return sms, newtext, n1
        #return c, n
            #c = cc.split()

    c = allf(text, n)

    data = {
       'res': c
       #'count': counter
    }
    return JsonResponse(data)

# проверка на соответвие 80% и вывод

'''def proc(count, partc, info):
    if count> partc*0.8:
        c="Данный текст не зашифрован! Зашифруйте его :)"
    else:
       # proc(test("dfgd fgbfb fgtbrf", partc),partc)
        c = "Текст зашифрован :( Скорее всего со смещением - n"
        c= dec(info,1)

    return c'''









